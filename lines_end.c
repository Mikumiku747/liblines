/* Daniel Selmes 2017 */

#include "lines.h"
#include <cairo.h>
#include <stdlib.h>

/*
lines_end
linesct *context: The line drawing context to be closed down. 
--------
Closes down a line drawing context, freeing up the resources it was using.
*/
void lines_end(linesct *context)
{
	cairo_destroy(context->cairo_context);
	cairo_surface_destroy(context->cairo_surface);
	free(context);
	return;
}
