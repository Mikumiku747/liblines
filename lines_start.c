/* Daniel Selmes 2017 */

#include "lines.h"
#include <stdlib.h>

/*
lines_start
int width: The width of the canvas.
int height: The height of the canvas.
Returns linesct *: A line drawing context.
--------
Creates a new line drawing context (including the backend stuff for libcairo
and the like.
*/
linesct *lines_start(int width, int height)
{
	/* Create the cario surface and context. */
	linesct *context = malloc(sizeof(linesct));
	context->cairo_surface = cairo_image_surface_create(
		CAIRO_FORMAT_ARGB32, width, height);
	context->cairo_context = cairo_create(context->cairo_surface);
	/* Clear the screen. */
	cairo_set_source_rgb(context->cairo_context, 0.0, 0.0, 0.0);
	cairo_paint(context->cairo_context);
	/* Set initial "Brush" paramters. */
	cairo_set_source_rgb(context->cairo_context, 0.0, 1.0, 0.0);
	cairo_set_line_width(context->cairo_context, 2);
	/* Fill in the rest of the context details. */
	context->color[0] = 0.0; context->color[1] = 1.0; context->color[2] = 0.0;
	context->stroke = 2;
	context->width = width;
	context->height = height;
	/* Return the built context. */
	return context;	
}
