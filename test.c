/*
test.c
Daniel Selmes 2017
Test functions for the lines library.
*/

#include "lines.h"
#include <stdio.h>

int main(int argc, char **argv)
{
	/* Coordinates for a square. */
	int squarecoords[] = {100, 100, 100, 300, 300, 300, 300, 100};
	/* Make sure we can save the test image. */
	if (argc < 2) {
		fprintf(stderr, "FAIL: No output filename was provided!\n");
		return -1;
	}
	/* Just do a nice simple test where we draw a boxed sqaure. */
	linesct *ct = lines_start(400, 400);
	lines_line(ct, 100, 100, 300, 300);
	lines_line(ct, 100, 300, 300, 100);
	lines_poly(ct, 4, squarecoords);
	lines_save(ct, argv[1]);
	lines_end(ct);
	printf("PASS: All tests successful.\n");
	return 0;
}
