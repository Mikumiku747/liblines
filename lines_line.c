/* Daniel Selmes 2017 */

#include "lines.h"
#include <cairo.h>

/*
lines_line
linesct *context: Line drawing context to draw on.
int startx: Starting X coordinate. 
int starty: Starting Y coordinate. 
int endx: Ending X coordinate. 
int endy: Ending Y coordinate. 
--------
Draws a line from the starting point to the finishing point described. Uses the
current color and stroke settings set in the drawing context.
*/
void lines_line(linesct *context, int startx, int starty, int endx, int endy)
{
	cairo_move_to(context->cairo_context, startx, starty);
	cairo_line_to(context->cairo_context, endx, endy);
	cairo_stroke(context->cairo_context);
	return;
}
