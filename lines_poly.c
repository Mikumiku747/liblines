/* Daniel Selmes 2017. */

#include "lines.h"
#include <cairo.h>

/*
lines_poly
linesct *context: The drawing context to draw the polygon on.
int count: The number of points in the polygon.
int *points: An array of points to use as the vertices of the polygon.
--------
Constructs a polygon from a set of points. The array of points is a linear array
of X,Y pairs for the vertices of the polygon. Lines are drawn starting at the
first vertex, with each line connecting to the next point, and the last point
connecting around to the first one.
*/
void lines_poly(linesct *context, int count, int *points)
{
	/* Draw the starting point. */
	cairo_move_to(context->cairo_context, points[0], points[1]);
	/* Draw each of the remaining points. */
	for (int vertex = 1; vertex < count; vertex++) {
		cairo_line_to(context->cairo_context,
			points[vertex*2], points[vertex*2+1]);
	}
	/* Close the path and perform the stroke. */
	cairo_close_path(context->cairo_context);
	cairo_stroke(context->cairo_context);
	return;
}
