/* Daniel Selmes 2017. */

#include "lines.h"
#include <cairo.h>

/*
lines_save
linesct *context: The line drawing context to save a picture of.
char *filename: The name of the file to save to (saves in PNG format).
--------
Saves an image of the current line drawing context to a file. Saves in PNG
format. Will save over an existing file.
*/
void lines_save(linesct *context, char *filename)
{
	cairo_surface_write_to_png(context->cairo_surface, filename);
	return;
}
