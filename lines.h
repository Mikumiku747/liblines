/*
liblines
Daniel Selmes 2017
Line graphics library using libcairo.
*/

#include <cairo.h>

/* Context Management. */
struct linecontext {
	cairo_surface_t *cairo_surface;
	cairo_t *cairo_context;
	int width;
	int height;
	float color[3];
	int stroke;
};
typedef struct linecontext linesct;
linesct *lines_start(int width, int height);
void lines_end(linesct *context);

/* Drawing Functions. */
void lines_line(linesct *context, int startx, int starty, int endx, int endy);
void lines_poly(linesct *context, int count, int *points);

/* Saving Functions. */
void lines_save(linesct *context, char *filename);
