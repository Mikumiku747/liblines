# makefile
# Daniel Selmes 2017
# Makefile for building the lines library

SOURCES := $(shell find . -name "*.c" -a -not -name "test.c")
OBJS := $(patsubst %.c, %.o, $(SOURCES))

PKGCONF_LIBS := $(shell pkg-config --cflags --libs cairo)
CFLAGS_INCL := 
CFLAGS_OPTS := -Wall -Wextra -g -std=c11

CFLAGS = $(CFLAGS_OPTS) $(CFLAGS_INCL) $(PKGCONF_LIBS)

all: liblines.a test
	./test test.png

liblines.a: $(OBJS)
	$(AR) -cr liblines.a $(OBJS)

test: test.c liblines.a
	$(CC) $(CFLAGS) -o test test.c -L. -llines

.PHONY: clean

clean:
	rm -rfv *.o *.a
	rm -rfv test test.png
